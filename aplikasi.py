# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 15:20:25 2018

@author: yolanda
"""


import re
import csv
import pandas as pd
import string
from string import digits
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from collections import Counter
import matplotlib as mpl
import matplotlib.pyplot as plt
from subprocess import check_output
from wordcloud import WordCloud
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cross_validation import train_test_split
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from pandas import *
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn import linear_model
import numpy as np

#Loading data
pd.set_option('display.max_colwidth',-1)
data = pd.read_csv('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Data/Data Train/data_train.csv',dtype=str,delimiter=';',encoding='utf-8')
print ('Banyak data =', len(data))
data

# Data Labeling
# In[3]:

neg = len([x for x in data['Sentiment'] if x == '-1'])
pos = len([x for x in data['Sentiment'] if x == '1'])
net = len([x for x in data['Sentiment'] if x == '0'])
neg, pos, net


# Data Preprocessing
# ## Pre-Processing Training Set and Validation
# ### 1. Case Folding
# Convert text into lower case

# In[4]:

pd.set_option('display.max_colwidth',-1)
for i in range(len(data)):
    lowercase = str.lower(data['Text'].iloc[i])
    data['Text'].iloc[i]=lowercase
print ('Data train case folding...')
data

print (data.head)


# ### 2. Normalization
# Convert words into normalized words, convert negation, and remove punctuations.

# In[5]:

# Convert words into normalized words
normalization = csv.reader(open('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Corpus/normalisasi.csv', 'r'))

d = {}
for row in normalization:
    k,v= row
    d[str.lower(k)] = str.lower(v)
pat = re.compile(r"\b(%s)\b" % "|".join(d))

for i in range(len(data)):
    text = str.lower(data['Text'].iloc[i])
    normal = pat.sub(lambda m: d.get(m.group()), text)
    data['Text'].iloc[i]=normal 
print ('Data train normalization...')
data

print (data)


# Convert Negation
negation = csv.reader(open('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Corpus/convert negation.csv', 'r'))

d = {}
for row in negation:
    k,v= row
    d[str.lower(k)] = str.lower(v)
    #print d[k]
pat = re.compile(r"\b(%s)\b" % "|".join(d))
for i in range(len(data)):
    convert = str.lower(data['Text'].iloc[i])
    convert = pat.sub(lambda m: d.get(m.group()), convert)
    data['Text'].iloc[i]=convert

print(data)

# Remove Punctuations
cleaned=[]
i=0
for words in data['Text']:
    for punctuation in string.punctuation:
        words = words.replace(punctuation,"")
    for number in '1234567890':
        words = words.replace(number,"")
    data['Text'][i] = words
    i = i+1
print ('Data train punctuation removing...')
data

print(data)


# ### 3. Stopwords Removing
# Remove stopwords from text

# In[8]:

cachedStopWords = set(stopwords.words("english"))
for i in range (len(data)):
    meaningful = data['Text'].iloc[i]
    meaningful = " ".join([word for word in meaningful.split() if word not in cachedStopWords])
    data['Text'].iloc[i]=meaningful
print ('Data trainstop words removing...')
data


print (data)

# ### 4. Tokenizing
# Change words into tokens

# In[9]:
stop_words=set(stopwords.words('english'))
tokens = []
for words in data['Text']:
    word_tokens = word_tokenize(words)
    words=[]
    for word in ( w for w in word_tokens if not w in stop_words):
        words.append(word)
    tokens.append(words)
print ('Showing results of data train tokenization...\n')
tokens

print(tokens)


# ### 5. Stemming
# Change words into stemmed words in the Porter Stemmer list

# In[10]:

cleandata = []
for words in tokens:
    word=[]
    for i in range (0, len(words)):
        ps = PorterStemmer()
        review = [ps.stem(words[i])]
        review = ' '.join(review)
        word.append(review)
    cleandata.append(word)
print ('Showing result of data train stemming...')
cleandata

i=0
for words in cleandata:
    sentences=' '.join(words)
    data['Text'][i]=sentences
    i=i+1
print ('Showing cleaned data train...')
data

print (data)



# ## Count Sentences (menghitung jumlah kalimat yang sama )

# In[11]:

sentences_counts = Counter(data['Text'])
print  ( 'Count the number of sentences...\n')
print (sentences_counts)

# ## Count Words(menghitung jumlah kata yang sama)

# In[12]:

words_count=Counter()

for words in cleandata:    
    wordfreq= words_count.update(words)
print ('Count the number of words...\n')
print (words_count)

# In[13]:

wordcloud=WordCloud(background_color='white',max_words=50, random_state=40).generate(str(cleandata))
print ('Showing Word cloud of data train... \n')
print (wordcloud)
plt.imshow(wordcloud)
plt.axis('off')
plt.show()



# ## Feature Extraction
# ### 1. Bag of Words CountVectorizer
# 

# ###### Note: CountVectorizers count the number of words that are in the corpus. 

# In[14]:

countVect = CountVectorizer(ngram_range=(1,1))
data_countVect = countVect.fit_transform(data['Text']) 

print ("Number of reviews, number of terms : ", data_countVect.shape)


# In[15]:

print("Number of features : %d \n" %len(countVect.get_feature_names()))
print("Show some feature names : \n", countVect.get_feature_names()[::100])


# In[16]:

print ('Showing the vector of vocabularies...')
countVect.vocabulary_
print(countVect.vocabulary)


# In[17]:

print ('Creating data train Bag of Words...\n')
print ("{} sentences and {} unique words create a matrix of the shape {}.".format(
    len(data['Text']),
    len(countVect.get_feature_names()),
    data_countVect.toarray().shape
))
#Create the bag of words matrix
data_countVect_BoW = data_countVect.toarray()
data_countVect_BoW
print (data_countVect_BoW)


# In[18]:

print ('Split data into 80% training set and 20% validation set...')
X_train, X_val, y_train, y_val = train_test_split(data['Text'], data['Sentiment'], test_size=0.2, random_state=2)
print ('\nLoad %d training set...\n' %(X_train.shape[0]))
print ('Show a review in the training set:\n', X_train.iloc[1])
print ('\nLoad %d validation set...\n' %(X_val.shape[0]))
print ('Show a review in the validation set:\n', X_val.iloc[1])

# In[19]:

print ('Showing the list of data labels...')
sentiment_list = list(y_train.value_counts().index)
sentiment_list

print(sentiment_list)

# In[20]:

X_train_countVect=countVect.fit_transform(X_train)
print ("Number of data train, number of terms : ", X_train_countVect.shape)

# In[21]:

X_val_countVect=countVect.transform(X_val)
print ("Number of validation set, number of terms:", X_val_countVect.shape)

# In[22]: untuk data train

neg=len([x for x in y_train if x == u'-1'])
pos=len([x for x in y_train if x == u'1'])
net=len([x for x in y_train if x == u'0'])
neg, pos, net

print (neg,pos,net)

# In[23]: untuk data validation set

neg=len([x for x in y_val if x == u'-1'])
pos=len([x for x in y_val if x == u'1'])
net=len([x for x in y_val if x == u'0'])
neg, pos, net

print (neg,pos,net)

# ### 2. TF IDF
# Fit and transform the training data to a document-term matrix using TfidfVectorizer

# In[24]:

print ('Fit and transform the training data to a document-term matrix using TfidfVectorizer...')
tfidf = TfidfVectorizer(min_df=2) #minimum document frequency of 2\
X_train_tfidf = tfidf.fit_transform(X_train)
print (X_train_tfidf)


# In[25]:

print("Number of features : %d \n" %len(tfidf.get_feature_names()))
print("Show some feature names : \n", tfidf.get_feature_names()[::1])

# In[26]:

print ('Fit and transform the training data to a document-term matrix using TfidfTransformer...')
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_countVect)
X_train_tfidf.shape

print(X_train_tfidf.shape)


# In[27]:

print ("{} sentences and {} unique words create a matrix of the train shape {}.".format(
    len(data),
    len(tfidf.get_feature_names()),
    X_train_tfidf.toarray().shape
))
X_train_tfidf.toarray()

print(X_train_tfidf.toarray())

# In[28]:

X_val_tfidf = tfidf_transformer.transform(X_val_countVect)
X_val_tfidf.shape

print(X_val_tfidf.shape)


# In[29]:

print ("{} sentences and {} unique words create a matrix of the validation shape {}.".format(
    len(data),
    len(tfidf.get_feature_names()),
    X_val_tfidf.toarray().shape
))
X_val_tfidf.toarray()

print(X_val_tfidf.toarray())



# In[30]:

# Print model evaluation to predicted result
def modelEvaluation(predictions):
    print ("\nAccuracy on validation set: {:.4f}".format(accuracy_score(y_val, predictions)))
    print ("\nClassification report : \n", metrics.classification_report(y_val, predictions))
    print ( "\nConfusion Matrix : \n", metrics.confusion_matrix(y_val, predictions))
    
    
    # ## Training Models

# #### 1. Multinomial Naive Bayes

# In[31]:

print ("Training the Naive Bayes...\n")

mnb = MultinomialNB().fit(X_train_tfidf, y_train)
mnb_predictions = mnb.predict(X_val_tfidf)
print (mnb)
    
# ### Model Classification

# #### Naive Bayes

# In[34]:

print ("Naive Bayes prediction results...\n")
print (mnb_predictions)
    
# In[35]:

print ("Naive Bayes evaluation results...\n")
modelEvaluation(mnb_predictions)
print(modelEvaluation(mnb_predictions))



# # Classification Novel Data

# ### Loading Data Test

# # Classification Novel Data

# ### Loading Data Test

# In[41]:

pd.set_option('display.max_colwidth',-1)
samsung_data = pd.read_csv('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Data/Data Test/Samsung_1.csv', dtype=str, delimiter=';', encoding='utf-8')
print ('Loading Samsung Reviews Data...')
print ('Total Data =', len(samsung_data))
samsung_data.head()
print(samsung_data.head())


# In[42]:

pd.set_option('display.max_colwidth',-1)
iphone_data = pd.read_csv('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Data/Data Test/Iphone_1.csv', dtype=str, delimiter=';', encoding='utf-8')
print ('Loading Iphone Reviews Data...')
print ('Total Data =', len(iphone_data))
iphone_data.head()
print(iphone_data.head())


# In[43]:

pd.set_option('display.max_colwidth',-1)
xiaomi_data = pd.read_csv('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Data/Data Test/Xiaomi_1.csv', dtype=str, delimiter=';', encoding='utf-8')
print ('Loading Xiaomi Reviews Data...')
print ('Total Data =', len(xiaomi_data))
xiaomi_data.head()





# ### Preprocessing

# #### Case Folding

# In[44]:

# In[44]:

for i in range(len(samsung_data)):
    lowercase = str.lower(samsung_data['Text'].iloc[i])
    samsung_data['Text'].iloc[i]=lowercase
print ('Samsung Review Data Case folding...')
samsung_data.head()

print(samsung_data.head())



# In[45]:

for i in range(len(iphone_data)):
    lowercase = str.lower(iphone_data['Text'].iloc[i])
    iphone_data['Text'].iloc[i]=lowercase
print ('Iphone Review Data Case folding...' )   
iphone_data.head()

print(iphone_data.head())


# In[46]:

for i in range(len(xiaomi_data)):
    lowercase = str.lower(xiaomi_data['Text'].iloc[i])
    xiaomi_data['Text'].iloc[i]=lowercase
print ('Xiaomi Review Data Case folding...')
xiaomi_data.head()

print(xiaomi_data.head())


# ### 3. Normalization
# Convert words into normalized words, convert negation, and remove punctuations.

# In[47]:

# Convert words into normalized words
normalization = csv.reader(open('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Corpus/normalisasi.csv', 'r'))

d = {}
for row in normalization:
    k,v= row
    d[str.lower(k)] = str.lower(v)
pat = re.compile(r"\b(%s)\b" % "|".join(d))

# In[48]:

for i in range(len(samsung_data)):
    text = str.lower(samsung_data['Text'].iloc[i])
    normal = pat.sub(lambda m: d.get(m.group()), text)
    samsung_data['Text'].iloc[i]=normal 
print ('Samsung Review Data normalization...')
samsung_data.head()

print(samsung_data.head())


# In[49]:

for i in range(len(iphone_data)):
    text = str.lower(iphone_data['Text'].iloc[i])
    normal = pat.sub(lambda m: d.get(m.group()), text)
    iphone_data['Text'].iloc[i]=normal 
print ('Iphone Review Data normalized...')   
iphone_data.head()



# In[50]:

for i in range(len(xiaomi_data)):
    text = str.lower(xiaomi_data['Text'].iloc[i])
    normal = pat.sub(lambda m: d.get(m.group()), text)
    xiaomi_data['Text'].iloc[i]=normal 
print ('Xiaomi Review Data normalized...')   
xiaomi_data.head()

# In[51]:

# Convert Negation
negation = csv.reader(open('E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Corpus/convert negation.csv', 'r'))

d = {}
for row in negation:
    k,v= row
    d[str.lower(k)] = str.lower(v)
    #print d[k]
pat = re.compile(r"\b(%s)\b" % "|".join(d))

# In[52]:

for i in range(len(samsung_data)):
    convert = str.lower(samsung_data['Text'].iloc[i])
    convert = pat.sub(lambda m: d.get(m.group()), convert)
    samsung_data['Text'].iloc[i]=convert
print ('Samsung Review Data negation converting...')
samsung_data.head()

print(samsung_data.head())

# In[53]:

for i in range(len(iphone_data)):
    convert = str.lower(iphone_data['Text'].iloc[i])
    convert = pat.sub(lambda m: d.get(m.group()), convert)
    iphone_data['Text'].iloc[i]=convert
print ('Iphone Review Data negation converting...' ) 
iphone_data.head()

# In[54]:

for i in range(len(xiaomi_data)):
    convert = str.lower(xiaomi_data['Text'].iloc[i])
    convert = pat.sub(lambda m: d.get(m.group()), convert)
    xiaomi_data['Text'].iloc[i]=convert
print ('Xiaomi Review Data negation converting...'  )
xiaomi_data.head()


# In[55]:

# Remove Punctuations
cleaned=[]
i=0
for words in samsung_data['Text']:
    for punctuation in string.punctuation:
        words = words.replace(punctuation,"")
    for number in '1234567890':
        words = words.replace(number,"")
    samsung_data['Text'][i] = words
    i = i+1
print ('Samsung Review Data punctuation removing...' )
samsung_data.head()
print(samsung_data.head())


# In[56]:

cleaned=[]
i=0
for words in iphone_data['Text']:
    for punctuation in string.punctuation:
        words = words.replace(punctuation,"")
    for number in '1234567890':
        words = words.replace(number,"")
    iphone_data['Text'][i] = words
    i = i+1
print ('Iphone Review Data punctuation removing...' )
iphone_data.head()
print(iphone_data.head())

# In[57]:

cleaned=[]
i=0
for words in xiaomi_data['Text']:
    for punctuation in string.punctuation:
        words = words.replace(punctuation,"")
    for number in '1234567890':
        words = words.replace(number,"")
    xiaomi_data['Text'][i] = words
    i = i+1
print ('Xiaomi Review Data punctuation removing...' )
xiaomi_data.head()

# In[58]:

cachedStopWords = set(stopwords.words("english"))
for i in range (len(samsung_data)):
    meaningful = samsung_data['Text'].iloc[i]
    meaningful = " ".join([word for word in meaningful.split() if word not in cachedStopWords])
    samsung_data['Text'].iloc[i]=meaningful
print ('Samsung Review Data Stop words removing...' )   
samsung_data.head()

print(samsung_data.head())


# In[59]:

for i in range (len(iphone_data)):
    meaningful = iphone_data['Text'].iloc[i]
    meaningful = " ".join([word for word in meaningful.split() if word not in cachedStopWords])
    iphone_data['Text'].iloc[i]=meaningful
print ('Iphone Review Data Stop words removing...') 
iphone_data.head()


# In[60]:

for i in range (len(xiaomi_data)):
    meaningful = xiaomi_data['Text'].iloc[i]
    meaningful = " ".join([word for word in meaningful.split() if word not in cachedStopWords])
    xiaomi_data['Text'].iloc[i]=meaningful
print ('Xiaomi Review Data Stop words removing...')   
xiaomi_data.head()


# ### 4. Tokenizing
# Change words into tokens

# In[61]:

stop_words=set(stopwords.words('english'))
samsung_tokens = []
for words in samsung_data['Text']:
    word_tokens = word_tokenize(words)
    words=[]
    for word in ( w for w in word_tokens if not w in stop_words):
        words.append(word)
    samsung_tokens.append(words)
print ('Samsung Review Data tokenizing...') 
print (samsung_tokens)


stop_words=set(stopwords.words('english'))
iphone_tokens = []
for words in iphone_data['Text']:
    word_tokens = word_tokenize(words)
    words=[]
    for word in ( w for w in word_tokens if not w in stop_words):
        words.append(word)
    iphone_tokens.append(words)
print ('Iphone Review Data tokenizing...' )
print (iphone_tokens)

# In[63]:

stop_words=set(stopwords.words('english'))
xiaomi_tokens = []
for words in xiaomi_data['Text']:
    word_tokens = word_tokenize(words)
    words=[]
    for word in ( w for w in word_tokens if not w in stop_words):
        words.append(word)
    xiaomi_tokens.append(words)
print ('Xiaomi Review Data tokenizing...') 
print (xiaomi_tokens)

# ### 5. Stemming
# Change words into stemmed words in the Porter Stemmer list

# In[64]:

samsung_cleandata = []
for words in samsung_tokens:
    word=[]
    for i in range (0, len(words)):
        ps = PorterStemmer()
        review = [ps.stem(words[i])]
        review = ' '.join(review)
        word.append(review)
    samsung_cleandata.append(word)
print ('Samsung Review Data stemming...') 
print (samsung_cleandata)

# In[65]:

iphone_cleandata = []
for words in iphone_tokens:
    word=[]
    for i in range (0, len(words)):
        ps = PorterStemmer()
        review = [ps.stem(words[i])]
        review = ' '.join(review)
        word.append(review)
    iphone_cleandata.append(word)
print ('Iphone Review Data stemming...') 
print (iphone_cleandata)


# In[66]:

xiaomi_cleandata = []
for words in xiaomi_tokens:
    word=[]
    for i in range (0, len(words)):
        ps = PorterStemmer()
        review = [ps.stem(words[i])]
        review = ' '.join(review)
        word.append(review)
    xiaomi_cleandata.append(word)
print ('Xiaomi Review Data stemming...') 
print (xiaomi_cleandata)

# In[67]:

i=0
for words in samsung_cleandata:
    sentences=' '.join(words)
    samsung_data['Text'][i]=sentences
    i=i+1
print ('Showing result of Preprocessed Samsung data...')
samsung_data.head()
print(samsung_data.head())


# In[68]:

i=0
for words in iphone_cleandata:
    sentences=' '.join(words)
    iphone_data['Text'][i]=sentences
    i=i+1
print ('Showing result of Preprocessed Iphone data...')
iphone_data.head()


# In[69]:

i=0
for words in xiaomi_cleandata:
    sentences=' '.join(words)
    xiaomi_data['Text'][i]=sentences
    i=i+1
print ('Showing result of Preprocessed xiaomi data...')
xiaomi_data.head()


# In[70]:

X_train_new=data['Text']
y_train_new=data['Sentiment']
print ('Loading %d training set...\n'%(X_train_new.shape[0]))
print ('Showing some training set examples...')
X_train_new.head()

# In[71]:

X_samsung=samsung_data['Text']
y_samsung=samsung_data['Sentiment']
print( 'Loading %d Samsung dataset...\n'%(X_samsung.shape[0]))
print ('Showing some training set examples...')
X_samsung.head()

print(X_samsung.head())

# In[72]:

X_iphone=iphone_data['Text']
y_iphone=iphone_data['Sentiment']
print ('Loading %d Iphone dataset...\n'%(X_iphone.shape[0]))
print ('Showing some training set examples...')
X_iphone.head()

print(X_iphone.head())


# In[73]:

X_xiaomi=xiaomi_data['Text']
y_xiaomi=xiaomi_data['Sentiment']
print ('Loading %d Xiaomi dataset...\n'%(X_xiaomi.shape[0]))
print ('Showing some training set examples...')
X_xiaomi.head()

print(X_xiaomi.head())

# ### 2. Feature Extraction
# #### TF-IDF Vectorizer

# In[74]:

tfidf = TfidfVectorizer(min_df=2) #minimum document frequency of 2
X_train_new_tfidf = tfidf.fit_transform(X_train_new)

print("Number of features : %d \n" %len(tfidf.get_feature_names()))
print("Show some feature names : \n", tfidf.get_feature_names()[::1])

# In[75]:

countVect = CountVectorizer(ngram_range=(1,1))
X_train_new_countVect = countVect.fit_transform(X_train_new) 
X_train_new_countVect.shape

# In[76]:

tfidf_new_transformer = TfidfTransformer()
X_train_new_tfidf = tfidf_new_transformer.fit_transform(X_train_new_countVect)
X_train_new_tfidf.shape


# In[77]:

X_samsung_countVect = countVect.transform(X_samsung)
X_samsung_tfidf = tfidf_transformer.fit_transform(X_samsung_countVect)
X_samsung_tfidf.shape


# In[78]:

X_iphone_countVect = countVect.transform(X_iphone)
X_iphone_tfidf = tfidf_transformer.fit_transform(X_iphone_countVect)
X_iphone_tfidf.shape



# In[79]:

X_xiaomi_countVect = countVect.transform(X_xiaomi)
X_xiaomi_tfidf = tfidf_transformer.fit_transform(X_xiaomi_countVect)
X_xiaomi_tfidf.shape

# ### 3. Training data train

# #### 1. Multinomial Naive Bayes

# In[80]:

mnb = MultinomialNB().fit(X_train_new_tfidf, y_train_new)
mnb_samsung_predictions = mnb.predict(X_samsung_tfidf)
mnb_iphone_predictions = mnb.predict(X_iphone_tfidf)
mnb_xiaomi_predictions = mnb.predict(X_xiaomi_tfidf)

# In[84]:

csvfile = "E:\KULIAH\SEMESTER V/CERTAN/Project/Proyek/Data/Result/mnb_samsung_predictions.csv"
with open(csvfile, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(mnb_samsung_predictions)
    
    # In[85]:

samsung_neg_mnb = len([x for x in mnb_samsung_predictions if x == u'-1'])
samsung_netral_mnb = len([x for x in mnb_samsung_predictions if x == u'0'])
samsung_pos_mnb = len([x for x in mnb_samsung_predictions if x == u'1'])
samsung_neg_mnb,samsung_netral_mnb,samsung_pos_mnb
print(samsung_neg_mnb,samsung_netral_mnb,samsung_pos_mnb)



# In[86]:

samsung_BR_mnb = float(samsung_pos_mnb - samsung_neg_mnb)
samsung_BR_mnb
print(samsung_BR_mnb)

# In[87]:

samsung_NBR_mnb= (samsung_BR_mnb/(samsung_pos_mnb + samsung_neg_mnb))*100
samsung_NBR_mnb
print(samsung_NBR_mnb)

# #### Iphone

# In[98]:

mnb_iphone_predictions


# In[99]:

csvfile = "E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Data/Result/mnb_iphone_predictions.csv"
with open(csvfile, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(mnb_iphone_predictions)

# In[100]:

iphone_neg_mnb = len([x for x in mnb_iphone_predictions if x == u'-1'])
iphone_netral_mnb = len([x for x in mnb_iphone_predictions if x == u'0'])
iphone_pos_mnb = len([x for x in mnb_iphone_predictions if x == u'1'])
iphone_neg_mnb,iphone_netral_mnb,iphone_pos_mnb

print(iphone_neg_mnb,iphone_netral_mnb,iphone_pos_mnb)


# In[101]:

iphone_BR_mnb = float(iphone_pos_mnb - iphone_neg_mnb)
iphone_BR_mnb
print(iphone_BR_mnb)


# In[102]:

iphone_NBR_mnb= (iphone_BR_mnb/(iphone_pos_mnb + iphone_neg_mnb))*100
iphone_NBR_mnb


# In[113]:

mnb_xiaomi_predictions


# In[114]:

csvfile = "E:/KULIAH/SEMESTER V/CERTAN/Project/Proyek/Data/Result/mnb_xiaomi_predictions.csv"
with open(csvfile, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(mnb_xiaomi_predictions)
    
    
    # In[115]:

xiaomi_neg_mnb = len([x for x in mnb_xiaomi_predictions if x == u'-1'])
xiaomi_netral_mnb = len([x for x in mnb_xiaomi_predictions if x == u'0'])
xiaomi_pos_mnb = len([x for x in mnb_xiaomi_predictions if x == u'1'])
xiaomi_neg_mnb,xiaomi_netral_mnb,xiaomi_pos_mnb


# In[116]:

xiaomi_BR_mnb = float(xiaomi_pos_mnb - xiaomi_neg_mnb)
xiaomi_BR_mnb


# In[117]:

xiaomi_NBR_mnb= (xiaomi_BR_mnb/(xiaomi_pos_mnb + xiaomi_neg_mnb))*100
xiaomi_NBR_mnb


# ### Visualization

# In[128]:

N=1
ind = np.arange(N)
width=0.25
fig, ax = plt.subplots()

samsung = (samsung_NBR_mnb)
samsung_std = (2)
rects1 = ax.bar(ind, samsung, width, color='Blue', yerr=samsung_std)

iphone = (iphone_NBR_mnb)
iphone_std = (3)
rects2 = ax.bar(ind + width, iphone, width, color='green', yerr=iphone_std)

xiaomi = (xiaomi_NBR_mnb)
xiaomi_std = (3)
rects3 = ax.bar(ind + width + width, xiaomi, width, color='yellow', yerr=xiaomi_std)

ax.set_ylabel('Scores')
ax.set_title('Scores group by algorithm')
ax.set_xticks(ind + width/2)
ax.set_xticklabels(('MNB'))
ax.legend((('Samsung','Iphone','Xiaomi')), loc=(0, -.38))



def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/1.25,1.025*height,'%d'%int(height), ha='center',va='bottom')   
autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
plt.show

print(plt.show)








