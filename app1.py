# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 13:44:10 2018

@author: Personal
"""

# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}
app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='Sentiment Analisis Handphone Branding',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

     html.Div(children='Dash Data Visualization', style={
        'textAlign': 'center',
        'color': colors['text']
    }),
    

    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': [1], 'y': [87], 'type': 'bar', 'name': 'Iphone'},
                {'x': [2], 'y': [88], 'type': 'bar', 'name': 'Samsung' },
                 {'x': [3], 'y': [90], 'type': 'bar', 'name': 'Xiaomi' },
            ],
            'layout': {
                'plot_bgcolor': colors['background'],
                'paper_bgcolor': colors['background'],
                'font': {
                    'color': colors['text']
            }
                }
                
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)