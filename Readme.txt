#Sentiment Analisis Handphone Branding dengan Dash Application sebaga GUI untuk memvisualisasikan data

## Dash Tutorials
This application was created in conjunction with the [**Dash tutorial series**](https://pythonprogramming.net/data-visualization-application-dash-python-tutorial-introduction/).
1. import dash
2. import dash_core_components as dcc
3. import dash_html_components as html
4. Open app1.py untuk menampilkan GUI


## Quick start

Preparation:
Install Anaconda2
Install Python2
Install Lybrary yang dibutuhkan
Pindahkan seluruh file yang ada di dalam file Data dan Corpus ke dekstop.

Cara menjalankan program:
1. Buka aplikasi Anaconda Navigator
2. Pilih Jupyter Notebook yang terdapat pada halaman awal Anaconda Navigator
3. Tekan tombol Launch. Jupyter notebook akan ditampilkan pada web browser.
4. Untuk mengupload kode program, tekan tombol upload yang terdapat pada halaman Home Jupyter Notebook.
5. Pilih file yang terdapat pada file Program
6. Tekan tombol upload
7. File berhasil di load, pilih kode program untuk membukanya pada tab baru
8. Buatlah file baru dengan cara menekan tombol New yang terdapat pada halaman Home
9. Salin setiap kode program yang diawali dengan # In[Number] pada setiap satu kolom halaman editor
10. Untuk menjalankan kode program satu kolom dapat dilakukan dengan menekan tombol Shift+Enter
11. Setelah semua kolom disalin, jalankan program dengan memilih menu Kernel-> Restart & Run all
12. Tunggu beberapa saat program berjalan untuk menampilkan hasil

Terima kasih.